//
// Created by Andrey Gostev on 15.03.2022.
//

#ifndef PL1_HEAP_H
#define PL1_HEAP_H

#include "vector.h"

template<class Arg>
class heap : public vector<Arg>{
private:
    vector<Arg> Vec;

public:
    heap() : Vec() {}

    void insert(Arg arg) {
        if (!this->Vec.size()) {
            this->Vec.push_back(arg);
        } else {
            auto it = this->Vec.end();

        }
    }
};

#endif //PL1_HEAP_H
