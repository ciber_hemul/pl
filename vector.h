//
// Created by Andrey Gostev on 15.03.2022.
//

#ifndef PL1_VECTOR_H
#define PL1_VECTOR_H

#include <iostream>

#define TRY_CATCH(expr, error, error_type, message) try{if(!expr){throw error;}} catch(error_type){std::cout<<message;exit(-1);} // message must be char*, handler must be void handler_name(char*)

// ERRORS
#define BAD_POSITION    0x0
#define BAD_POINTER     0x1

template<class Arg>
class vector {
private:
    int Size;
    int MaxLength;
    Arg* Vec;

public:
    vector() : Size(0), MaxLength(10), Vec(new Arg[10]) {}

    vector(Arg* BegPtr, Arg* EndPtr) {
        TRY_CATCH(EndPtr < BegPtr, BAD_POINTER, int, "Bad pointers. Cannot create vector\n")
        this->Size = EndPtr - BegPtr;
        this->MaxLength = this->Size + 10;
        this->Vec = new Arg[this->MaxLength];
    }

    vector(int Size) {
        this->Size = Size;
        this->MaxLength = this->Size + 10;
        this->Vec = new Arg[this->MaxLength];
    }

    ~vector() {
        delete [] this->Vec;
    }

    void insert(Arg arg, int pos) {
        pos--;
        TRY_CATCH(pos < 0 || pos > this->Size, BAD_POSITION, int, "Bad position to insert argument\n");
        if (this->Size == this->MaxLength) {
            this->MaxLength += 10;

            Arg* newVec = new Arg[this->MaxLength];

            memcpy_s(newVec, sizeof(Arg) * pos, this->Vec, sizeof(Arg) * pos);
            *(newVec + pos) = arg;
            memcpy_s(newVec + pos + 1, sizeof(Arg) * (this->Size - pos), this->Vec + pos, sizeof(Arg) * (this->Size - pos));

            delete [] this->Vec;

            this->Vec = newVec;
        } else {
            memcpy_s(this->Vec + pos + 1, sizeof(Arg) * (this->Size - pos), this->Vec + pos, sizeof(Arg) * (this->Size - pos));
            *(this->Vec + pos) = arg;
        }

        this->Size++;
    }

    void push_back(Arg arg) {
        this->insert(arg, this->Size + 1);
    }

    Arg pop_back() {
        this->Size--;
        return *(this->Vec + this->Size);
    }
    
    Arg *begin() {
        return this->Vec;
    }
    
    Arg *end() {
        return this->Vec + this->Size;
    }
    
    friend std::ostream &operator<<(std::ostream &out, vector<Arg> &vec) {
        for(auto &el : vec) {
            out << el << " ";
        }

        return out;
    }

    int size() {
        return this->Size;
    }

    Arg back() {
        return *(this->Vec + this->Size - 1);
    }
};

#endif //PL1_VECTOR_H
